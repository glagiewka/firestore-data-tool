# firestore-data-tool

[![Build status](https://gitlab.com/gLagiewka/firestore-data-tool/badges/master/build.svg)](https://gitlab.com/gLagiewka/firestore-data-tool/commits/master)
[![Coverage](https://gitlab.com/gLagiewka/firestore-data-tool/badges/master/coverage.svg)](https://gLagiewka.gitlab.io/firestore-data-tool)
[![License](https://img.shields.io/github/license/mashape/apistatus.svg)](https://gitlab.com/gLagiewka/firestore-data-tool/blob/master/LICENSE.md)

A data backup and population tool for Firebase Cloud Firestore database.

---

## Installing

When using yarn:
```js
yarn add firestore-data-tool
```

When using npm:
```js
npm install firestore-data-tool
```

---

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/gLagiewka/firestore-data-tool/blob/master/LICENSE.md) file for details.
