import { DocumentReference, GeoPoint } from "@google-cloud/firestore";


/**
 * The Firestore document's data.
 */
export type FirestoreData = { [index: string]: FirestoreValue };


/**
 * Type of the document's data fields.
 */
export type FirestoreValue = null
  | boolean
  | number
  | string
  | Date
  | GeoPoint
  | DocumentReference
  | FirestoreData
  | FirestoreData[];
