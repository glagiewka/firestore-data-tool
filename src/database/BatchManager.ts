import {
  DocumentData,
  DocumentReference,
  FieldPath,
  Firestore,
  Precondition,
  SetOptions,
  UpdateData,
  WriteBatch,
} from "@google-cloud/firestore";


/**
 * Wraps an WriteBatch instance and tracks the amount of write operation executed on it.
 */
export class BatchManager implements WriteBatch {
  private _size: number = 0;
  private batch: WriteBatch;

  constructor(db: Firestore) {
    this.batch = db.batch();
  }

  get batchSize() {
    return this._size;
  }


  public create(documentRef: DocumentReference, data: DocumentData): WriteBatch {
    this._size += 1;

    return this.batch.create(documentRef, data);
  }


  public set(documentRef: DocumentReference, data: DocumentData, options?: SetOptions | undefined): WriteBatch {
    this._size += 1;

    return this.batch.set(documentRef, data, options);
  }


  public update(documentRef: DocumentReference, data: UpdateData, precondition?: Precondition | undefined): WriteBatch;
  public update(
    documentRef: DocumentReference,
    field: string | FieldPath,
    value: any,
    ...fieldsOrPrecondition: any[],
  ): WriteBatch;
  public update(documentRef: any, data: any, precondition?: any, rest?: any[]) {
    this._size += 1;

    return this.batch.update(documentRef, data, precondition);
  }


  public delete(documentRef: DocumentReference, precondition?: Precondition | undefined): WriteBatch {
    this._size += 1;

    return this.batch.delete(documentRef, precondition);
  }


  public commit(): Promise<FirebaseFirestore.WriteResult[]> {
    return this.batch.commit();
  }
}
