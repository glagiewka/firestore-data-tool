import { DocumentReference, Firestore, Precondition, SetOptions } from "@google-cloud/firestore";
import { FirestoreData } from "src";
import { isObject } from "src/utils/object";
import { Dictionary } from "src/utils/other";


const BATCH_ITEM_TYPES = ["create", "delete", "set", "update"];


type BatchItemCreate = {
  data: FirestoreData,
  type: "create",
};


type BatchItemDelete = {
  precondition?: Precondition,
  type: "delete",
};


type BatchItemSet = {
  data: FirestoreData,
  setOptions?: SetOptions,
  type: "set",
};


type BatchItemUpdate = {
  data: FirestoreData,
  precondition?: Precondition,
  type: "update",
};


/**
 * Represents a Firestore document reference and the data associated with it.
 */
export type BatchItem = (BatchItemCreate | BatchItemSet | BatchItemDelete | BatchItemUpdate) & {
  /**
   * The firestore document reference.
   */
  docRef: DocumentReference,
};


/**
 * Checks whether th given value is possibly a BatchItem object.
 */
export const isBatchItem = (value: any, db: Firestore): value is BatchItem => {
  if (!isObject(value) || !isObject(value.docRef)) {
    return false;
  }

  const type: string = value.type;

  if (BATCH_ITEM_TYPES.indexOf(type) === -1) {
    return false;
  }

  const hasData = isObject(value.data);

  switch (type) {
    case "create":
      return hasData;

    case "delete":
      return checkPrecondition(value);

    case "set":
      return hasData && checkSetOptions(value);

    case "update":
      return hasData && checkPrecondition(value);
  }

  return false;
};


const checkPrecondition = (value: Dictionary<any>) => {
  const hasPrecondition = value.precondition;

  return !hasPrecondition || hasPrecondition && isObject(value.precondition);
};



const checkSetOptions = (value: Dictionary<any>) => {
  const hasSetOptions = value.setOptions;

  return !hasSetOptions || hasSetOptions && isObject(value.setOptions);
};
