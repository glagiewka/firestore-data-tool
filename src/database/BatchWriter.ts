import { Firestore } from "@google-cloud/firestore";
import { getLast, isEmpty } from "src/utils/array";
import { Writable } from "stream";
import { BatchItem, BatchManager, isBatchItem } from ".";


/**
 * The maximum number of writes a batch can contain before Firebase throws an error.
 */
export const MAX_WRITES_IN_BATCH = 500;


/**
 * Number of concurrent batch commits. Infinity means that all of the batches will be committed at the same time.
 */
export const CONCURRENT_BATCHES = Infinity;


export type BatchWriterOptions = {
  /**
   * The size of the batches the collections should be split to. (defaults to 500)
   */
  maxBatchSize?: number,

  /**
   * Maximum number of batches tha are concurrently committed. (defaults to unlimited)
   */
  concurrentBatches?: number,
};


/**
 * A write stream that pushes the incoming Firestore data into the database.
 */
export class BatchWriter extends Writable {
  /**
   * A list of batches awaiting for a commit.
   */
  private batchList: BatchManager[] = [];

  private maxBatchSize: number;
  private concurrentBatches: number;


  /**
   * @param db      The Firestore database object.
   * @param options The options.
   */
  constructor(private readonly db: Firestore, {
    concurrentBatches = CONCURRENT_BATCHES,
    maxBatchSize = MAX_WRITES_IN_BATCH,
  }: BatchWriterOptions = {}) {
    super({
      objectMode: true,
    });

    this.maxBatchSize = maxBatchSize;
    this.concurrentBatches = concurrentBatches;
  }


  public _write(chunk: BatchItem, encoding: string, callback: (error?: Error) => void) {
    if (!isBatchItem(chunk, this.db)) {
      callback(new Error("The given chunk is not a BatchItem object."));
      return;
    }

    if (isEmpty(this.batchList) || getLast(this.batchList)!.batchSize === this.maxBatchSize) {
      this.batchList.push(new BatchManager(this.db));
    }

    const currentBatch = getLast(this.batchList)!;

    this.addItemToBatch(chunk, currentBatch);

    if (currentBatch.batchSize === this.maxBatchSize && this.batchList.length === this.concurrentBatches) {
      this.commitBatches(callback);
    } else {
      callback();
    }
  }


  public _final(callback: (error?: Error) => void) {
    if (!isEmpty(this.batchList)) {
      this.commitBatches(callback);
    } else {
      callback();
    }
  }


  /**
   * Pushes every batch into the database.
   *
   * @param callback  A callback called when the batches has been pushed to the database or the
   *  database returned an error.
   */
  private commitBatches(callback: (error?: Error) => void) {
    Promise
      .all(this.batchList.map(batch => batch.commit()))
      .then(() => callback())
      .catch(callback);

    this.batchList = [];
  }


  /**
   * Calls the appropriate batch method based on the type of the writeItem.
   *
   * @param batchItem   An object describing an item to save into the database.
   * @param batch       The batch the item should  be added to.
   */
  private addItemToBatch(batchItem: BatchItem, batch: BatchManager) {
    switch (batchItem.type) {
      case "create":
        batch.create(batchItem.docRef, batchItem.data);
        break;

      case "delete":
        batch.delete(batchItem.docRef, batchItem.precondition);
        break;

      case "set":
        batch.set(batchItem.docRef, batchItem.data, batchItem.setOptions);
        break;

      case "update":
        batch.update(batchItem.docRef, batchItem.data, batchItem.precondition);
        break;
    }
  }
}
