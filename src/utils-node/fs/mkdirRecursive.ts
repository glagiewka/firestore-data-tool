import { mkdir, stat } from "fs";
import { isAbsolute, join, sep } from "path";
import { isEmpty } from "src/utils/string";
import { promisify } from "util";


const statPromise = promisify(stat);
const mkdirPromise = promisify(mkdir);


/**
 * Creates directories along the given path.
 *
 * @param path  The path to create directories for.
 * @param mode  Node mkdir's mode parameter.
 */
export const mkdirRecursive = async (path: string, mode?: string | number | null | undefined) => {
  const pathParts = path.trim().split(sep);
  let currentPath = "";

  if (isAbsolute(path) && isEmpty(pathParts[0])) {
    currentPath = sep;
    pathParts.shift();
  }

  for (const pathPart of pathParts) {
    currentPath = join(currentPath, pathPart);

    try {
      await mkdirPromise(currentPath, mode);
    } catch (error) {
      if (error.code === "EEXIST") {
        const stats = await statPromise(currentPath);

        if (stats.isDirectory()) {
          continue;
        }
      }

      throw error;
    }
  }
};
