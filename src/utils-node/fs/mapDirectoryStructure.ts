import { basename, relative, sep } from "path";
import { set } from "src/utils/object";
import { Dictionary } from "src/utils/other";
import { traverseDirectory } from ".";


export type DirectoryMap = {
  /**
   * Represents the structure of a directory.
   */
  structure: Dictionary<string | object>;

  /**
   * An absolute path of the directory this map represents.
   */
  path: string;
};


export type MapDirectoryStructureOptions = {
  /**
   * If set to true, ignores dotted files.
   */
  noDotted?: boolean,

  /**
   * If set to true, ignores empty directories.
   */
  noEmpty?: boolean,
};


/**
 * Maps the directory at the given path to an object representing the directory structure.
 *
 * @param rootPath    The path to the directory.
 * @param options     The options.
 */
export const mapDirectoryStructure = async (rootPath: string, {
  noDotted = false,
  noEmpty = false,
}: MapDirectoryStructureOptions = {}): Promise<DirectoryMap> => {
  const dirStructure: Dictionary<any> = {};

  await traverseDirectory(rootPath, {
    onDirectory(dirEvt) {
      if (
        noEmpty
        || relative(rootPath, dirEvt.path) === "" // Do not insert an empty object at the root level
      ) {
        return;
      }

      set(dirStructure, relative(rootPath, dirEvt.path).split(sep), {});
    },

    onFile(fileEvt) {
      if (noDotted && basename(fileEvt.path).startsWith(".")) {
        return;
      }

      set(dirStructure, relative(rootPath, fileEvt.path).split(sep), fileEvt.path);
    },
  });

  return {
    path: rootPath,
    structure: dirStructure,
  };
};
