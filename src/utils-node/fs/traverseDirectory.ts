import { exists, readdir, stat, Stats } from "fs";
import { join, resolve } from "path";
import { promisify } from "util";


const existsPromise = promisify(exists);
const readdirPromise = promisify(readdir);
const statPromise = promisify(stat);


export type TraverseDirectoryOptions = {
  /**
   * Called for each directory.
   */
  onDirectory?: (evt: TraverseEvent) => void,

  /**
   * Called at the end of traversal.
   */
  onEnd?: () => void,

  /**
   * Called for each file.
   */
  onFile?: (evt: TraverseEvent) => void,

  /**
   * Called at the start of traversal.
   */
  onStart?: () => void,
};


export type TraverseEvent = {
  path: string,
  stats: Stats,
};


/**
 * Traverses the given directory recursively.
 *
 * @param rootPath  The path to the directory.
 * @param options   The options.
 */
export const traverseDirectory = async (rootPath: string, options?: TraverseDirectoryOptions) => {
  const rootExists = await existsPromise(rootPath);

  if (!rootExists) {
    throw new Error("No file or directory exists at the given path.");
  }

  const rootStats = await statPromise(rootPath);

  if (!rootStats.isDirectory()) {
    throw new Error("The given path doesn't point to a directory.");
  }

  callStart(options);

  await handleDirectory(rootPath, rootStats, options);

  callEnd(options);
};


const traverseRecursively = async (path: string, options?: TraverseDirectoryOptions): Promise<void> => {
  const stats = await statPromise(path);

  if (stats.isDirectory()) {
    callDirectory(path, stats, options);

    await handleDirectory(path, stats, options) as any;
  }

  if (stats.isFile()) {
    callFile(path, stats, options);
  }
};


const handleDirectory = async (dirPath: string, dirStat: Stats, options?: TraverseDirectoryOptions) => {
  const dirContents = await readdirPromise(resolve(dirPath));

  return Promise.all(dirContents.map(item => {
    return traverseRecursively(join(dirPath, item), options);
  }));
};


const callDirectory = (dirPath: string, dirStats: Stats, options?: TraverseDirectoryOptions) => {
  options && options.onDirectory && options.onDirectory({ path: dirPath, stats: dirStats });
};


const callFile = (filePath: string, fileStats: Stats, options?: TraverseDirectoryOptions) => {
  options && options.onFile && options.onFile({ path: filePath, stats: fileStats });
};


const callEnd = (options?: TraverseDirectoryOptions) => {
  options && options.onEnd && options.onEnd();
};


const callStart = (options?: TraverseDirectoryOptions) => {
  options && options.onStart && options.onStart();
};
