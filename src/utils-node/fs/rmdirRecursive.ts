import { rmdir, unlink } from "fs";
import { join } from "path";
import { isObject, ownKeyValues } from "src/utils/object";
import { Dictionary } from "src/utils/other";
import { promisify } from "util";
import { mapDirectoryStructure } from ".";


const unlinkPromise = promisify(unlink);
const rmdirPromise = promisify(rmdir);


/**
 * Recursively removes the given directory.
 *
 * @param path  The directory to remove.
 */
export const rmdirRecursive = async (path: string) => {
  const dirStructure = (await mapDirectoryStructure(path)).structure;

  await removeDirContents(dirStructure, path);
};


const removeDirContents = async (dirStructure: Dictionary<any>, rootPath: string) => {
  for (const [pathPart, pathDirStructure] of ownKeyValues(dirStructure)) {
    if (isObject(pathDirStructure)) {
      const dirPath = join(rootPath, pathPart);

      await removeDirContents(pathDirStructure, dirPath);
      await rmdirPromise(dirPath);
    } else {
      await unlinkPromise(pathDirStructure);
    }
  }
};
