/**
 * Checks whether the given array is empty.
 */
export const isEmpty = <TItem>(array: TItem[]) => {
  return !array.length;
};
