/**
 * Returns the last element from the given array.
 */
export const getLast = <TItem>(array: TItem[]) => {
  return array.length ? array[array.length - 1] : undefined;
};
