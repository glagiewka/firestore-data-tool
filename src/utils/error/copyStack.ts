/**
 * Copies stack from the given source error to the given target error.
 */
export const copyStack = <TTargetError extends Error>(sourceError: Error, targetError: TTargetError) => {
  targetError.stack = sourceError.stack;

  return targetError;
};
