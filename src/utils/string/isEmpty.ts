/**
 * Checks whether the given string is empty, i.e. has length of 0.
 */
export const isEmpty = (value: string) => {
  return value.length === 0;
};
