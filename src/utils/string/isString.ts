/**
 * Checks if the given value is an object.
 */
export const isString = (value: any): value is string => {
  return typeof value === "string";
};
