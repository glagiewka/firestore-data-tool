export type Dictionary<TValue> = { [index: string]: TValue };
