import { isObject } from ".";


const OBJECT_PROTO = Object.prototype;


/**
 * Checks if the given value is a plain object.
 */
export const isPlainObject = (value: any): value is object => {
  if (!isObject) {
    return false;
  }

  const valueProto = (value as any).__proto__;

  return valueProto === undefined || valueProto === OBJECT_PROTO;
};
