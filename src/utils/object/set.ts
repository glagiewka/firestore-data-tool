import { Dictionary } from "src/utils/other";
import { isObject } from ".";


/**
 * Sets the given value at the given path inside the given object.
 */
export function set(object: object, path: string | string[], value: any): Dictionary<any>;
export function set(object: Dictionary<any>, path: string | string[], value: any) {
  const pathParts = Array.isArray(path) ? path : path.split(".");
  let currentLevel = object;

  pathParts.forEach((pathPart, index) => {
    const arrIndex = getIndex(pathPart);
    const lastPart = index === pathParts.length - 1;

    if (arrIndex !== -1) {
      pathPart = arrIndex;
    }

    if (lastPart) {
      currentLevel[pathPart] = value;
      return;
    }


    const isNextPartArray = getIndex(pathParts[index + 1]) > -1;

    if (isNextPartArray && !Array.isArray(currentLevel[pathPart])) {
      currentLevel[pathPart] = [];
    }

    if (!isNextPartArray && !isObject(currentLevel[pathPart])) {
      currentLevel[pathPart] = lastPart ? value : {};
    }

    currentLevel = currentLevel[pathPart];
  });

  return object;
}


const getIndex = (str: string) => {
  const match = /^\[(0|[1-9]\d*)\]$/.exec(str);

  return match ? match[1] : -1;
};
