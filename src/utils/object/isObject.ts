import { Dictionary } from "src/utils/other";


/**
 * Checks if the given value is an object.
 */
export const isObject = (value: any): value is Dictionary<any> => {
  return Boolean(value) && typeof value === "object";
};
