/**
 * Yields own enumerable keys and corresponding values of the given value.
 */
export function* ownKeyValues<TValue extends { [index: string]: any }>(
  obj: TValue,
): IterableIterator<[string, typeof obj[keyof TValue]]> {
  for (const key in Object(obj)) {
    if (Object.hasOwnProperty.call(obj, key)) {
      yield [key, obj[key]];
    }
  }
}
