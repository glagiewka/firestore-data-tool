import { isPlainObject, ownKeyValues } from ".";


/**
 * Recursively yields own enumerable keys and corresponding values of the given value.
 */
export function* ownKeyValuesRecursive<TObject extends object>(
  object: TObject,
): IterableIterator<[string, TObject[keyof TObject]]> {
  const stack: any[] = [object];

  while (stack.length) {
    const level = stack.pop()!;

    for (const [key, value] of ownKeyValues(level)) {
      yield [key, value];

      if (isPlainObject(value)) {
        stack.push(value);
      }
    }
  }
}
