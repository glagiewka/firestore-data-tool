/**
 * Yields each value from the given iterable after it has been modified by the given callback.
 */
export function* map<TValue, TYieldValue>(
  iterable: Iterable<TValue>,
  callback: (value: TValue) => TYieldValue,
): IterableIterator<TYieldValue> {
  for (const value of iterable) {
    yield callback(value);
  }
}
