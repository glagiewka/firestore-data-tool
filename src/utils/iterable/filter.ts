/**
 * Yields each value from the given iterable if the predicate functions returns true.
 */
export function* filter<TValue>(
  iterable: Iterable<TValue>,
  predicate: (value: TValue) => boolean,
): IterableIterator<TValue> {
  for (const value of iterable) {
    if (predicate(value)) {
      yield value;
    }
  }
}
