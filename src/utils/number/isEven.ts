/**
 * Checks whether the given number is even.
 */
export const isEven = (value: number) => {
  return (value % 2) === 0;
};
