import { writeFile } from "fs";
import { dirname, join, resolve } from "path";
import { FileItem, isFileItem } from "src/fs/FileItem";
import { mkdirRecursive } from "src/utils-node/fs";
import { Writable } from "stream";
import { promisify } from "util";


const writeFilePromise = promisify(writeFile);


/**
 * A write stream that writes the incoming Firestore data on the disk.
 */
export class DirectoryWriter extends Writable {
  /**
   * @param dirPath  The path to a directory the documents should be written to.
   */
  constructor(private readonly dirPath: string) {
    super({
      objectMode: true,
    });

    this.dirPath = resolve(this.dirPath);
  }


  public async _write(chunk: FileItem, encoding: string, callback: (error?: Error) => void) {
    if (!isFileItem(chunk)) {
      callback(new Error("The given chunk is not a FileItem object."));
      return;
    }

    try {
      const filePath = join(this.dirPath, `${chunk.path}.json`);

      await mkdirRecursive(dirname(filePath));
      await writeFilePromise(filePath, JSON.stringify(chunk.data));
      callback();
    } catch (error) {
      callback(error);
    }
  }

  public _final(callback: (error?: Error) => void) {
    callback();
  }
}
