/**
 * Represents an error that occurs during file system operations.
 */
export class FSError extends Error {
  constructor(public readonly message: string, public readonly path: string) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
