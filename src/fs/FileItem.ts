import { FirestoreData } from "src";
import { isEven } from "src/utils/number";
import { isObject } from "src/utils/object";
import { isString } from "src/utils/string";


/**
 * Represents a Firestore document path and the data associated with it.
 */
export type FileItem = {
  /**
   * The Firestore data object.
   */
  data: FirestoreData,

  /**
   * The path of the data.
   */
  path: string,
};


/**
 * Checks whether th given value is possibly a FileItem object.
 */
export const isFileItem = (value: any): value is FileItem => {
  return isObject(value)
    && isObject(value.data)
    && isString(value.path)
    && isEven(value.path.split("/").length);
};
