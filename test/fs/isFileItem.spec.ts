import { assert } from "chai";
import { isFileItem } from "src/fs";


describe("isFileItem()", () => {
  it("should return false when not given an object", () => {
    assert.isFalse(isFileItem(1));
  });


  it("should return false when given a FileItem without the data property", () => {
    assert.isFalse(isFileItem({
      path: "collection/doc1",
    }));
  });


  it("should return false when given a FileItem without the path property", () => {
    assert.isFalse(isFileItem({
      data: {},
    }));
  });


  it("should return false when given a FileItem with the path property that is not a string", () => {
    assert.isFalse(isFileItem({
      data: {},
      path: 1,
    }));
  });


  it("should return false when given a FileItem with the data property that is not an object", () => {
    assert.isFalse(isFileItem({
      data: 1,
      path: "collection/doc1",
    }));
  });


  it("should return false when given a FileItem with the path property that doesn't represent a \
correct Firestore document path", () => {
      assert.isFalse(isFileItem({
        data: {},
        path: "collection/doc1/subCollection",
      }));
    });


  it("should return true when given a correct FileItem", () => {
    assert.isTrue(isFileItem({
      data: {},
      path: "collection/doc1",
    }));
  });
});
