import { assert } from "chai";
import { tmpdir } from "os";
import { join } from "path";
import { DirectoryWriter } from "src/fs";
import { mapDirectoryStructure, rmdirRecursive } from "src/utils-node/fs";
import { anyOfClass, spy, verify } from "ts-mockito/lib/ts-mockito";


const TEST_DIR = join(tmpdir(), "test");


describe("DirectoryWriter stream", () => {
  afterEach(async () => {
    await rmdirRecursive(TEST_DIR);
  });


  it("should emit an error when given a chunk that is not a FileItem object", () => {
    const writer = new DirectoryWriter(TEST_DIR);
    const instance = { errorCallback: (error: Error) => { return; } };
    const mock = spy(instance);

    writer
      .on("error", (error) => instance.errorCallback(error))
      .write({
        data: { a: 1 },
        path: "collection1",
      });

    verify(mock.errorCallback(anyOfClass(Error))).once();
  });


  it("should create directory for the given FileItem", (done) => {
    const writer = new DirectoryWriter(TEST_DIR);

    writer.write({
      data: {
        a: 12,
      },
      path: "collection1/doc",
    });

    writer.end(() => {
      mapDirectoryStructure(TEST_DIR)
        .then(dirMap => {
          assert.deepEqual(dirMap.structure, {
            collection1: {
              "doc.json": join(TEST_DIR, "collection1/doc.json"),
            },
          });

        })
        .then(done)
        .catch(done);
    });
  });


  it("should create directory for the given FileItem if writing to a sub-collection", (done) => {
    const writer = new DirectoryWriter(TEST_DIR);

    writer.write({
      data: {
        a: 12,
      },
      path: "collection/doc",
    });

    writer.write({
      data: {
        a: 12,
      },
      path: "collection/doc/subCollection/subDoc",
    });

    writer.end(() => {
      mapDirectoryStructure(TEST_DIR)
        .then(dirMap => {
          assert.deepEqual(dirMap.structure, {
            collection: {
              "doc": {
                subCollection: {
                  "subDoc.json": join(TEST_DIR, "collection/doc/subCollection/subDoc.json"),
                },
              },
              "doc.json": join(TEST_DIR, "collection/doc.json"),
            },
          });
        })
        .then(done)
        .catch(done);
    });
  });
});
