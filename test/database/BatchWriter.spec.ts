import { Firestore } from "@google-cloud/firestore";
import { BatchWriter, BatchWriterOptions } from "src/database";
import { createFirestoreMock, createWriteBatchMocks } from "test/factories";
import { anyOfClass, spy, verify, when } from "ts-mockito";


type CreateBatchWriterOptions = {
  db?: Firestore,
  options?: BatchWriterOptions,
};


const createBatchWriter = ({
  db = createFirestoreMock(),
  options = {},
}: CreateBatchWriterOptions = {}) => {
  return new BatchWriter(db, options);
};


describe("BatchWriter stream", () => {
  it("should emit an error when given a chunk that is not a BatchItem object", () => {
    const writer = createBatchWriter();
    const instance = { errorCallback: (error: Error) => { return; } };
    const mock = spy(instance);

    writer
      .on("error", (error) => instance.errorCallback(error))
      .write({
        data: { a: 1 },
      });

    verify(mock.errorCallback(anyOfClass(Error))).once();
  });


  it("should call the end callback when not given any data", (done) => {
    const writer = createBatchWriter();

    writer.end(() => {
      done();
    });
  });


  it("should put consumed objects into a single batch", (done) => {
    const writeBatchMocks = createWriteBatchMocks(1);
    const wbMock1 = writeBatchMocks[0].mock;
    const firestoreMock = createFirestoreMock({ writeBatchMocks });
    const writer = createBatchWriter({ db: firestoreMock });
    const batchItem1 = {
      data: { a: 1 },
      docRef: firestoreMock.collection("collection1").doc("1"),
      type: "create",
    };
    const batchItem2 = {
      data: { a: 2 },
      docRef: firestoreMock.collection("collection1").doc("2"),
      type: "create",
    };
    const batchItem3 = {
      data: { a: 3 },
      docRef: firestoreMock.collection("collection1").doc("3"),
      type: "create",
    };

    writer.write(batchItem1);
    writer.write(batchItem2);
    writer.write(batchItem3);

    writer.end(() => {
      verify(wbMock1.create(batchItem1.docRef, batchItem1.data)).once();
      verify(wbMock1.create(batchItem2.docRef, batchItem2.data)).once();

      const lastCreateCall = wbMock1.create(batchItem3.docRef, batchItem3.data);

      verify(lastCreateCall).once();
      verify(wbMock1.commit()).once();
      verify(wbMock1.commit()).calledAfter(lastCreateCall);
      done();
    });
  });


  it("should put consumed object into multiple batches and commit them concurrently", (done) => {
    const writeBatchMocks = createWriteBatchMocks(2);
    const wbMock1 = writeBatchMocks[0].mock;
    const wbMock2 = writeBatchMocks[1].mock;
    const firestoreMock = createFirestoreMock({ writeBatchMocks });
    const writer = createBatchWriter({
      db: firestoreMock,
      options: {
        maxBatchSize: 2,
      },
    });
    const batchItem1 = {
      data: { a: 1 },
      docRef: firestoreMock.collection("collection1").doc("1"),
      type: "create",
    };
    const batchItem2 = {
      data: { a: 2 },
      docRef: firestoreMock.collection("collection1").doc("2"),
      type: "create",
    };
    const batchItem3 = {
      data: { a: 3 },
      docRef: firestoreMock.collection("collection1").doc("3"),
      type: "create",
    };
    const batchItem4 = {
      data: { a: 4 },
      docRef: firestoreMock.collection("collection1").doc("4"),
      type: "create",
    };

    writer.write(batchItem1);
    writer.write(batchItem2);
    writer.write(batchItem3);
    writer.write(batchItem4);

    writer.end(() => {
      verify(wbMock1.create(batchItem1.docRef, batchItem1.data)).once();
      verify(wbMock1.create(batchItem2.docRef, batchItem2.data)).once();
      verify(wbMock2.create(batchItem3.docRef, batchItem3.data)).once();

      const lastCreateCall = wbMock2.create(batchItem4.docRef, batchItem4.data);

      verify(lastCreateCall).once();
      verify(wbMock1.commit()).once();
      verify(wbMock2.commit()).once();
      verify(wbMock1.commit()).calledAfter(lastCreateCall);
      verify(wbMock2.commit()).calledAfter(lastCreateCall);
      done();
    });
  });


  it("should put consumed object into multiple batches committed one after another", (done) => {
    const writeBatchMocks = createWriteBatchMocks(3);
    const wbMock1 = writeBatchMocks[0].mock;
    const wbMock2 = writeBatchMocks[1].mock;
    const wbMock3 = writeBatchMocks[2].mock;
    const firestoreMock = createFirestoreMock({ writeBatchMocks });
    const writer = createBatchWriter({
      db: firestoreMock,
      options: {
        concurrentBatches: 2,
        maxBatchSize: 2,
      },
    });
    const batchItem1 = {
      data: { a: 1 },
      docRef: firestoreMock.collection("collection1").doc("1"),
      type: "create",
    };
    const batchItem2 = {
      data: { a: 2 },
      docRef: firestoreMock.collection("collection1").doc("2"),
      type: "create",
    };
    const batchItem3 = {
      data: { a: 3 },
      docRef: firestoreMock.collection("collection1").doc("3"),
      type: "create",
    };
    const batchItem4 = {
      data: { a: 4 },
      docRef: firestoreMock.collection("collection1").doc("4"),
      type: "create",
    };
    const batchItem5 = {
      data: { a: 5 },
      docRef: firestoreMock.collection("collection1").doc("5"),
      type: "create",
    };

    writer.write(batchItem1);
    writer.write(batchItem2);
    writer.write(batchItem3);
    writer.write(batchItem4);
    writer.write(batchItem5);

    writer.end(() => {
      verify(wbMock1.create(batchItem1.docRef, batchItem1.data)).once();
      verify(wbMock1.create(batchItem2.docRef, batchItem2.data)).once();
      verify(wbMock2.create(batchItem3.docRef, batchItem3.data)).once();

      const batch2LastCall = wbMock2.create(batchItem4.docRef, batchItem4.data);
      const batch3LastCall = wbMock3.create(batchItem5.docRef, batchItem5.data);

      verify(batch2LastCall).once();
      verify(batch3LastCall).once();
      verify(wbMock1.commit()).once();
      verify(wbMock2.commit()).once();
      verify(wbMock3.commit()).once();
      verify(wbMock1.commit()).calledAfter(batch2LastCall);
      verify(wbMock2.commit()).calledAfter(batch2LastCall);
      verify(wbMock2.commit()).calledBefore(batch3LastCall);
      verify(wbMock3.commit()).calledAfter(batch3LastCall);
      done();
    });
  });


  it("should emit an error when the database returns an error", (done) => {
    const writeBatchMocks = createWriteBatchMocks(1);
    const wbMock1 = writeBatchMocks[0].mock;
    const firestoreMock = createFirestoreMock({ writeBatchMocks });
    const writer = createBatchWriter({ db: firestoreMock });
    const batchItem = {
      data: { a: 1 },
      docRef: firestoreMock.collection("collection1").doc("1"),
      type: "create",
    };
    const errorInstance = { errorCallback: (error: Error) => { return; } };
    const errorMock = spy(errorInstance);

    when(wbMock1.commit()).thenReject(new Error("Database test error."));

    writer
      .on("error", (error) => errorInstance.errorCallback(error))
      .end(batchItem, () => {
        verify(errorMock.errorCallback(anyOfClass(Error))).once();
        done();
      });
  });


  it("should write a BatchItem with type equal to delete", (done) => {
    const writeBatchMocks = createWriteBatchMocks(1);
    const wbMock1 = writeBatchMocks[0].mock;
    const firestoreMock = createFirestoreMock({ writeBatchMocks });
    const writer = createBatchWriter({ db: firestoreMock });
    const batchItem = {
      docRef: firestoreMock.collection("collection1").doc("1"),
      precondition: {},
      type: "delete",
    };

    writer
      .end(batchItem, () => {
        verify(wbMock1.delete(batchItem.docRef, batchItem.precondition)).once();
        done();
      });
  });


  it("should write a BatchItem with type equal to set", (done) => {
    const writeBatchMocks = createWriteBatchMocks(1);
    const wbMock1 = writeBatchMocks[0].mock;
    const firestoreMock = createFirestoreMock({ writeBatchMocks });
    const writer = createBatchWriter({ db: firestoreMock });
    const batchItem = {
      data: { a: 1 },
      docRef: firestoreMock.collection("collection1").doc("1"),
      setOptions: {},
      type: "set",
    };

    writer
      .end(batchItem, () => {
        verify(wbMock1.set(batchItem.docRef, batchItem.data, batchItem.setOptions)).once();
        done();
      });
  });


  it("should write a BatchItem with type equal to update", (done) => {
    const writeBatchMocks = createWriteBatchMocks(1);
    const wbMock1 = writeBatchMocks[0].mock;
    const firestoreMock = createFirestoreMock({ writeBatchMocks });
    const writer = createBatchWriter({ db: firestoreMock });
    const batchItem = {
      data: { a: 1 },
      docRef: firestoreMock.collection("collection1").doc("1"),
      precondition: {},
      type: "update",
    };

    writer
      .end(batchItem, () => {
        verify(wbMock1.update(batchItem.docRef, batchItem.data, batchItem.precondition)).once();
        done();
      });
  });
});
