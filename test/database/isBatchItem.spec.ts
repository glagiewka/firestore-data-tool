import { assert } from "chai";
import { isBatchItem } from "src/database";
import { createFirestoreMock } from "test/factories";


describe("isBatchItem()", () => {
  it("should return false when not given an object", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem(1, firestoreMock));
  });


  it("should return false when given a BatchItem without the docRef property", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      data: { a: 1 },
      type: "create",
    }, firestoreMock));
  });


  it("should return false when given a BatchItem with docRef that is not an object", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      data: { a: 1 },
      docRef: 1,
      type: "create",
    }, firestoreMock));
  });


  it("should return false when given a BatchItem without the type property", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      data: { a: 1 },
      docRef: firestoreMock.collection("c").doc("a"),
    }, firestoreMock));
  });


  it("should return false when given a BatchItem with invalid type property", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      data: { a: 1 },
      docRef: firestoreMock.collection("c").doc("a"),
      type: "invalid-type",
    }, firestoreMock));
  });


  it("should return true when given a correct BatchItem with type set to create", () => {
    const firestoreMock = createFirestoreMock();

    assert.isTrue(isBatchItem({
      data: { a: 1 },
      docRef: firestoreMock.collection("c").doc("a"),
      type: "create",
    }, firestoreMock));
  });


  it("should return false when given a create BatchItem without the data property", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      docRef: firestoreMock.collection("c").doc("a"),
      type: "create",
    }, firestoreMock));
  });


  it("should return true when given a delete BatchItem", () => {
    const firestoreMock = createFirestoreMock();

    assert.isTrue(isBatchItem({
      docRef: firestoreMock.collection("c").doc("a"),
      type: "delete",
    }, firestoreMock));
  });


  it("should return false when given a delete BatchItem with the precondition that is not an object", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      docRef: firestoreMock.collection("c").doc("a"),
      precondition: 1,
      type: "delete",
    }, firestoreMock));
  });


  it("should return true when given a set BatchItem", () => {
    const firestoreMock = createFirestoreMock();

    assert.isTrue(isBatchItem({
      data: { a: 1 },
      docRef: firestoreMock.collection("c").doc("a"),
      type: "set",
    }, firestoreMock));
  });


  it("should return false when given a set BatchItem without the data property", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      docRef: firestoreMock.collection("c").doc("a"),
      type: "set",
    }, firestoreMock));
  });


  it("should return false when given a set BatchItem with the setOptions that is not an object", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      data: { a: 1 },
      docRef: firestoreMock.collection("c").doc("a"),
      setOptions: 1,
      type: "set",
    }, firestoreMock));
  });


  it("should return true when given an update BatchItem", () => {
    const firestoreMock = createFirestoreMock();

    assert.isTrue(isBatchItem({
      data: { a: 1 },
      docRef: firestoreMock.collection("c").doc("a"),
      type: "update",
    }, firestoreMock));
  });


  it("should return false when given an update BatchItem without the data property", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      docRef: firestoreMock.collection("c").doc("a"),
      type: "update",
    }, firestoreMock));
  });


  it("should return false when given an update BatchItem with the precondition that is not an object", () => {
    const firestoreMock = createFirestoreMock();

    assert.isFalse(isBatchItem({
      data: { a: 1 },
      docRef: firestoreMock.collection("c").doc("a"),
      precondition: 1,
      type: "update",
    }, firestoreMock));
  });
});
