import { CollectionReference, DocumentReference, Firestore, WriteBatch } from "@google-cloud/firestore";
import { spy } from "ts-mockito";


export type MockInstance<TObject extends object> = {
  /**
   * The ts-mockito mock object.
   */
  mock: TObject,

  /**
   * The original object.
   */
  instance: TObject,
};


export type CreateFirestoreMockOptions = {
  /**
   * Number of batches that will be returned from Firestore's batch().
   */
  writeBatchMocks?: Array<MockInstance<WriteBatch>>,
};


/**
 * Creates a Firestore object mock with the collection function stubbed to return firestore collection reference mock.
 */
export const createFirestoreMock = ({
  writeBatchMocks = [],
}: CreateFirestoreMockOptions = {}) => {
  const wbSeq = writeBatchMocks[Symbol.iterator]();
  const firestoreMock = {
    batch() {
      const nextWriteBatchMock = wbSeq.next().value;

      if (!nextWriteBatchMock) {
        throw new Error("No registered WriteBatch mocks found.");
      }

      return nextWriteBatchMock.instance;
    },
    collection: createCollectionFunction(null),
  };

  return firestoreMock as Firestore;
};


/**
 * Creates an array of WriteBatch mocks.
 */
export function createWriteBatchMocks(count: 1): [MockInstance<WriteBatch>];
export function createWriteBatchMocks(count: 2): [MockInstance<WriteBatch>, MockInstance<WriteBatch>];
export function createWriteBatchMocks(
  count: 3,
): [MockInstance<WriteBatch>, MockInstance<WriteBatch>, MockInstance<WriteBatch>];

export function createWriteBatchMocks(count: number): Array<MockInstance<WriteBatch>>;
export function createWriteBatchMocks(count = 1) {
  const mocks: Array<MockInstance<WriteBatch>> = [];

  for (let mockIndex = 0; mockIndex < count; mockIndex++) {
    const instance = {
      commit() { return; },
      create() { return; },
      delete() { return; },
      set() { return; },
      update() { return; },
    } as any as WriteBatch;

    const mock = spy(instance) as any as WriteBatch;

    mocks.push({ mock, instance });
  }

  return mocks;
}


/**
 * Creates a Firestore doc function mock.
 */
export const createDocFunction = (parent: CollectionReference) => {
  return (path?: string) => {
    const documentReference = {
      parent,
      path: `${parent.path}/${path || "__UUID__"}`,
    } as DocumentReference;

    documentReference.collection = createCollectionFunction(documentReference);

    return documentReference;
  };
};


/**
 * Creates a Firestore collection function mock.
 */
export const createCollectionFunction = (parent: DocumentReference | null) => {
  return (path: string) => {
    const collectionReference = {
      parent,
      path: parent ? `${parent.path}/${path}` : path,
    } as CollectionReference;

    collectionReference.doc = createDocFunction(collectionReference);

    return collectionReference;
  };
};
